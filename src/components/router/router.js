import Vue from 'vue';
import VueRouter from 'vue-router';

import dChat from '../chat-room-window/chat-room-window.vue';
import Enter from '../enter-block/enter-block.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'enter',
        component: Enter,
        props: true
    },
    {
        path: '/:id',
        name: 'chat',
        component: dChat,
        props: true
    }
];

export default new  VueRouter ({
    routes
});