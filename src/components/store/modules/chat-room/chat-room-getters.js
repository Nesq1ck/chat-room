export default {
    LOADING: state => state.loading,
    
    PART_LOADING: state => state.partsLoad,
    
    LIST: state => state.list,
    
    MESSAGE: state => state.chatRoomMessage,
    
    NAME: state => state.name
}