export default {
    SET_LOADING (state, data) {
        state.loading = data
    },
    
    SET_LOADING_PARTS (state, data) {
      state.partsLoad = data;
    },
    
    SET_LIST_CHAT_ROOM (state, data) {
        state.list = data
    },
    
    SET_PARTS_CHAT_ROOM (state, data) {
        state.parts = data
    },
    
    SET_MESSAGE_CHAT_ROOM (state, data) {
        state.chatRoomMessage = data
    },
    
    SET_WRITE_MESSAGE (state, data) {
        const date = new Date();

        const obj = {
                    "id": data.id,
                    "user": true,
                    "name": "Василий",
                    "date": `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}.${date.getHours()}:${date.getMinutes()}`,
                    "message": data.message,
                    "loaded": false
                };
        state.chatRoomMessage.push(obj);
    },
    
    SET_NAME (state, data) {
        state.name = data
    },
    
    SET_NEW_ITEM_LOAD (state, data) {
        state.chatRoomMessage[data.index].loaded = data.status
    }
}