import axios from 'axios';

export default {
    /**
     * Получение данных при загрузке страницы и отдельного чата
     * @param id - id чата
     */
    GET_DATA_CHAT_ROOM ({ commit }){
    
        commit('SET_LOADING', true);
        axios.get('CHAT_ROOM_SUCCESS.json')
            .then(response => {
                response = response.data;
                commit('SET_LIST_CHAT_ROOM', response.data.list);
                commit('SET_PARTS_CHAT_ROOM', response.data.parts);
                commit('SET_NAME', response.data.name);
            })
            // eslint-disable-next-line no-console
            .catch(error => console.log(error))
            .finally(
                setTimeout(() => {
                    commit('SET_LOADING', false)
                }, 1000)
            )
    },
    
    GET_MESSAGE_CHAT_ROOM ({ state, commit }, id) {
        commit('SET_LOADING_PARTS', true);
        commit('SET_MESSAGE_CHAT_ROOM', state.parts[`parts${id}`]);
    
        setTimeout(() => {
            commit('SET_LOADING_PARTS', false)
        }, 1000);
    }
}