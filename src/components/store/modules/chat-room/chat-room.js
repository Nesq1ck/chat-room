import actions from './chat-room-actions';
import mutations from './chat-room-mutations';
import getters from './chat-room-getters';
import state from './chat-room-state';

export default {
    state,
    mutations,
    actions,
    getters,
}