import Vue from 'vue'
import App from './App.vue'

import router from './components/router/router.js';
import store from './components/store/store.js';

import { default as Vuedals } from 'vuedals';


Vue.use(Vuedals);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
