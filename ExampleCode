<template>
  <form
    class="plugin-settings-connected-plugin"
    @submit.prevent="$emit('submit')"
  >
    <HeadBlockPageSettings :title="$t('title')" :text="$t('text')" />

    <div class="plugin-settings-connected-plugin__main">
      <div class="plugin-settings-connected-plugin__search">
        <InputUI
          v-model.trim="search"
          :placeholder="$t('plugins.board.search')"
          hasClear
          class="plugin-settings-connected-plugin__search-input"
        >
          <template v-slot:leftInnerPlace>
            <SearchIcon class="plugin-settings-connected-plugin__search-icon" />
          </template>
        </InputUI>

        <LpButton
          :to="{ name: 'PluginsSettingsList' }"
          class="plugin-settings-connected-plugin__button-add-plugin"
        >
          {{ $t('addPlugin') }}
        </LpButton>
      </div>

      <LpTable
        :headers="tableHeaders"
        :loading="loading"
        :empty-text="$t('emptyText')"
        :items="filterPlugins ? filterPlugins : plugins"
        draggable
        @endDrag="sortedPlugins($event)"
      >
        <template #item.is_active="{value, item}">
          <ToggleUI
            :checked="value"
            :disabled="deleting"
            @change="updateStatePlugin(item.id, $event)"
          />
        </template>

        <template #item.created="{value}">
          {{ $options.formatDate(value, timezoneState) }}
        </template>

        <template #item.type="{value, item}">
          <div class="plugin-settings-connected-plugin__table-name">
            <img
              :src="$options.PLUGINS_ICONS[item.type]"
              alt=""
              class="plugin-settings-connected-plugin__icon-plugin"
            />

            <div class="plugin-settings-connected-plugin__table-title">
              {{ item.title }}
            </div>
          </div>
        </template>

        <template #item.actions="{item}">
          <component
            :is="deleting ? 'span' : 'RouterLink'"
            :to="{
              name: 'PluginSettingsConfiguringPlugin',
              params: {
                plugin: plugin(item.id),
                id: item.id,
                to: { name: 'PluginSettingsConnectedPlugin' },
              },
            }"
            class="plugin-settings-connected-plugin__action-button"
          >
            <SettingsIcon />
          </component>

          <div class="plugin-settings-connected-plugin__remove-button">
            <ConfirmDialog
              :theme="'red'"
              :offsetX="-70"
              @confirm="deletePlugin(item.id)"
            >
              <template #activator="{ on }">
                <button
                  :disabled="deleting"
                  class="plugin-settings-connected-plugin__action-button"
                  @click.stop="on"
                >
                  <TrashIcon />
                </button>
              </template>
            </ConfirmDialog>
          </div>
        </template>
      </LpTable>
    </div>
  </form>
</template>

<script>
  import api from '@/api';

  import { LpButton } from '@lpteam/ui';
  import ButtonUI from 'src/ui/ButtonUI';
  import ToggleUI from 'src/ui/ToggleUI';
  import InputUI from 'src/ui/InputUI';

  import LpTable from '@/ui/plp/LpTable/LpTable';

  import { PLUGINS_ICONS } from '@/utils/plugins';

  import TrashIcon from '@/components/icons/newUI/TrashIcon';
  import SearchIcon from 'src/components/icons/newUI/ProfilePluginsSettings/SearchIcon';

  import SettingsIcon from 'src/components/icons/newUI/ProfilePluginsSettings/SettingsIcon';

  import ConfirmDialog from 'src/ui/ConfirmDialog';

  import {
    formatDateWithTimezone,
    SERVER_DATE_FORMAT,
    SERVER_TIME_FORMAT,
  } from '@/services/time';
  import { mapState } from 'vuex';
  import { deepcopy, getTimezoneFromState } from '@/utils';
  import HeadBlockPageSettings from '../HeadBlockPageSettings';

  export default {
    name: 'PluginSettingsConnectedPlugin',

    data: () => ({
      password: false,
      ipAddress: false,
      showPluginList: false,
      plugins: [],
      searchValue: null,
      filterPlugins: null,
      loading: false,
      deleting: false,
      sorting: [],
    }),

    mounted() {
      this.getListPluginsCurrentPages();
    },

    components: {
      ButtonUI,
      ToggleUI,
      InputUI,

      TrashIcon,
      SearchIcon,
      SettingsIcon,

      ConfirmDialog,

      HeadBlockPageSettings,

      LpTable,
      LpButton,
    },

    PLUGINS_ICONS,

    formatDate(date, timezone) {
      return formatDateWithTimezone(
        date,
        timezone,
        `${SERVER_DATE_FORMAT} ${SERVER_TIME_FORMAT}`
      );
    },

    computed: {
      ...mapState('user', {
        timezoneState: getTimezoneFromState,
      }),
      tableHeaders() {
        return [
          {
            value: 'is_active',
            text: this.$t('status'),
            width: 50,
          },
          {
            value: 'id',
            text: 'ID',
            width: 100,
            align: 'center',
          },
          {
            value: 'created',
            text: this.$t('date'),
            width: 250,
          },
          {
            value: 'type',
            text: this.$t('name'),
          },
          {
            value: 'actions',
            text: this.$t('actions'),
            align: 'end',
          },
        ];
      },

      search: {
        get() {
          return this.searchValue;
        },
        set(value) {
          this.searchValue = value;

          this.filterPlugins = this.plugins.filter(plugin => {
            return (
              plugin.title
                .toUpperCase()
                .indexOf(this.searchValue.toUpperCase()) !== -1
            );
          });
        },
      },
    },

    methods: {
      async getListPluginsCurrentPages() {
        try {
          this.loading = true;

          const { response } = await api.plugins.getPlugins({
            url_params: { pageId: this.$route.params.pageId },
          });

          const filtered = response.sort(
            (pluginPrev, pluginNext) => pluginPrev.sort - pluginNext.sort
          );

          this.plugins = filtered;
        } catch (e) {
          //
        } finally {
          this.loading = false;
        }
      },

      plugin(id) {
        return this.plugins.find(plug => plug.id === id);
      },

      label(type) {
        return this.labels[type]?.icon;
      },

      async updateStatePlugin(id, value) {
        try {
          await api.plugins.updatePlugin({
            url_params: { pluginId: id },
            data: {
              is_active: value,
            },
          });

          const index = this.plugins.findIndex(plug => plug.id === id);

          this.plugins[index].is_active = value;
        } catch (e) {
          //
        }
      },

      async deletePlugin(id) {
        try {
          this.deleting = true;
          await api.plugins.deletePlugin({
            url_params: { pluginId: id },
          });

          const index = this.plugins.findIndex(plug => plug.id === id);

          this.plugins.splice(index, 1);
        } catch (e) {
          //
        } finally {
          this.deleting = false;
        }
      },

      sortedPlugins(value) {
        if (value.newIndex !== value.oldIndex) {
          const plugins = deepcopy(this.plugins);
          const oldItem = plugins[value.oldIndex];
          const newItem = plugins[value.newIndex];

          newItem.sort = value.oldIndex + 1;
          oldItem.sort = value.newIndex + 1;

          plugins[value.newIndex] = oldItem;
          plugins[value.oldIndex] = newItem;

          this.plugins = plugins;

          this.updateApiPlugins();
        }
      },

      updateApiPlugins() {
        const sorting = this.plugins.map(plugin => ({
          id: plugin.id,
          sort: plugin.sort,
        }));

        api.plugins.updatePositionPlugins({
          data: sorting,
        });
      },
    },
  };
</script>

<style lang="scss">
  .plugin-settings-connected-plugin {
    $root: &;

    width: 100%;
    position: relative;
    background-color: #fff;
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    padding: 30px;

    &__title {
      font-size: 18px;
      line-height: 21px;
      color: #263037;
      margin-bottom: 20px;
    }

    &__text {
      font-size: 14px;
      color: #52565f;
    }

    &__top-block {
      display: flex;
      justify-content: space-between;
      padding-bottom: 20px;
      border-bottom: 1px solid #eee;
      margin-bottom: 20px;
    }

    &__link {
      color: #2f82f0;
      text-decoration: underline;
    }

    &__close-btn {
      margin-left: 10px;
    }

    &__enable-file {
      color: $grey-7;
      transition: color 0.3s;

      &:hover {
        color: $grey-10;
      }
    }

    &__info {
      transition: color 0.3s;

      &:hover {
        color: $grey-10;
      }
    }

    &__buttons {
      display: flex;
      justify-content: flex-end;
      width: 100%;
    }

    &__main {
      margin-bottom: 20px;
    }

    &__label {
      color: $grey-7;
    }

    &__label-info {
      color: $grey-7;
    }

    &__toggle {
      margin-bottom: 15px;

      &:last-child {
        margin-bottom: 0;
      }

      &:hover {
        #{$root}__label {
          color: $grey-7;
          transition: color 0.3s;
        }
        #{$root}__label-info {
          color: $grey-7;
          transition: color 0.3s;
        }
      }
    }

    &__table-name {
      display: flex;
      align-items: center;
    }

    &__table-title {
      margin-left: 15px;
    }

    &__table-icon {
      max-width: 25px;
      width: 100%;
    }

    &__action-button {
      border: none;
      width: 20px;
      height: 20px;
      background-color: transparent;
      cursor: pointer;
      color: #838586;
      outline: none;

      &:hover {
        opacity: 0.8;
      }

      &:not(:last-child) {
        margin-right: 5px;
      }
    }

    &__search {
      margin-bottom: 15px;
      display: flex;
    }

    &__search-input {
      width: 100%;
      background: #f3f6f8;
      border-radius: 5px;
      position: relative;
      padding: 7px 0;
      flex: 1;
      margin-right: 15px;
    }

    &__empty-search {
      font-weight: normal;
      font-size: 16px;
      line-height: 19px;
      color: #6f7c88;
    }

    &__icon-plugin {
      max-width: 25px;
    }

    &__remove-button {
      display: inline-block;
    }

    &__drag-icon {
      cursor: move;
    }
  }
</style>

<i18n>
  {
    "en": {
      "title": "Plugins",
      "text": "Quick connection of services in a few clicks.",
      "addPlugin": "Add plugin",
      "back": "Return to plugin settings",
      "status": "Status",
      "date": "Connection date",
      "name": "Name",
      "actions": "Actions",
      "yandex": "Yandex",
      "google": "Google",
      "vk": "VKontakte",
      "facebook": "Facebook",
      "custom": "Custom code",
      "empty": "No matching plugins found",
      "emptyText": "Plugin not found."
    },
    "ru": {
      "title": "Плагины",
      "text": "Быстрое подключение различных программных блоков к вашему сайту для расширения возможностей и функционала - аналитика, чат, произвольный скрипт и другие",
      "addPlugin": "Добавить плагин",
      "back": "Вернуться к настройкам плагинов",
      "status": "Статус",
      "date": "Дата подключения",
      "name": "Название",
      "actions": "События",
      "yandex": "Яндекс метрика",
      "google": "Google аналитика",
      "vk": "VK пиксель",
      "facebook": "Facebook пиксель",
      "custom": "Произвольный код",
      "empty": "Подходящих плагинов не найдено",
      "emptyText": "Плагины не найдены."
    }
  }
</i18n>
